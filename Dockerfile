FROM rabbitmq:3.8.11-management-alpine

EXPOSE 5672
EXPOSE 15672

CMD ["rabbitmq-server"]